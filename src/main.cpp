#include <Arduino.h>
#include <TinyGPS++.h>
#include <Wire.h>
#include <string.h>
#include "HardwareSerial.h"
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "RTClib.h"
#include <LiquidCrystal.h>
 
#define RXD2 27
#define TXD2 26
#define RXD1 17
#define TXD1 16

HardwareSerial gpsSerial(1);
HardwareSerial sysSerial(2);
int LCDbacklightcontrol = 27;
int bufferEnablecontrol= 26;
int RTCdatetimesyncFlag = 1;
//String RTCDateUpdate;
//String RTCTimeUpdate;
String dateSDcard;
String dateSDcardCompare;
String SDfiletoreadSerial;
char SDfiletoreadSerialarray[15];
char dateSDcardarray[15];
char dataBuffer[300];
int SDfileReadingFlag;
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

const int rs = 32, en = 33, d4 = 0, d5 = 15, d6 = 2, d7 = 4;   // LCD Display
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
unsigned long previousMillisLCD = 0;
unsigned long currentMillisLCD =0;
const long intervalLCD = 2000;

///////////////////////////////Interuppt Pin decalarations/////////////////////////////

struct PulseInput {
  const uint8_t PIN;
  uint32_t numberOfPulses;
  bool pressed;
};

unsigned long previousMillis = 0; 
const long interval = 1000;

PulseInput pulseIn1 = {12, 0, false};
PulseInput pulseIn2 = {13, 0, false};
PulseInput pulseIn3 = {14, 0, false};
PulseInput pulseIn4 = {25, 0, false};
PulseInput pulseIn5 = {34, 0, false};
PulseInput pulseIn6 = {35, 0, false};
PulseInput pulseIn7 = {36, 0, false};
PulseInput pulseIn8 = {39, 0, false};
///////////////////////////// Hardware Interrupts calling/////////////////////////////////

void IRAM_ATTR isr1() {
  pulseIn1.numberOfPulses += 1;
  // pulseIn1.pressed = true;
}

void IRAM_ATTR isr2() {
  pulseIn2.numberOfPulses += 1;
  // pulseIn2.pressed = true;
}

void IRAM_ATTR isr3() {
  pulseIn3.numberOfPulses += 1;
  // pulseIn3.pressed = true;
}

void IRAM_ATTR isr4() {
  pulseIn4.numberOfPulses += 1;
  // pulseIn4.pressed = true;
}

void IRAM_ATTR isr5() {
  pulseIn5.numberOfPulses += 1;
  // pulseIn5.pressed = true;
}

void IRAM_ATTR isr6() {
  pulseIn6.numberOfPulses += 1;
  // pulseIn6.pressed = true;
}

void IRAM_ATTR isr7() {
  pulseIn7.numberOfPulses += 1;
  // pulseIn7.pressed = true;
}

void IRAM_ATTR isr8() {
  pulseIn8.numberOfPulses += 1;
  // pulseIn8.pressed = true;
}
///////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////Tiny GPS////////////////////////////////////////////////
// The TinyGPS++ object
TinyGPSPlus gps;

void RTCDateTimeSync(){     //For synching RTC Date and Time with GPS valid data 
  //if (gps.date.isValid() && gps.time.isValid()){
  if(gps.location.isValid() && gps.date.isValid() && gps.time.isValid()){
    RTCdatetimesyncFlag = 0;
   // Serial.println("TEST");
   // delay(2000);
    rtc.adjust(DateTime(gps.date.year(),gps.date.month(),gps.date.day(), gps.time.hour(), gps.time.minute(), gps.time.second()));
  }
}
//   char monthasstring[4] ;
//   monthasstring[0] = '\0';
//   if (gps.date.isValid() && gps.time.isValid()){
//     // Serial.print(gps.date.month());
//     // Serial.print(F("/"));
//     // Serial.print(gps.date.day());
//     // Serial.print(F("/"));
//     // Serial.print(gps.date.year());
//     switch (gps.date.month()){
//     case 1:
//       strcpy(monthasstring ,"Jan");
//       break;
//     case 2:
//       strcpy(monthasstring ,"Feb");
//       break;
//     case 3:
//       strcpy(monthasstring ,"Mar");
//       break;
//     case 4:
//       strcpy(monthasstring ,"Apr");
//       break;
//     case 5:
//       strcpy(monthasstring ,"May");
//       break;
//     case 6:
//       strcpy(monthasstring ,"Jun");
//       break;
//     case 7:
//       strcpy(monthasstring ,"Jul");
//       break;
//     case 8:
//       strcpy(monthasstring ,"Aug");
//       break;
//     case 9:
//       strcpy(monthasstring ,"Sep");
//       break;
//     case 10:
//       strcpy(monthasstring ,"Oct");
//       break;
//     case 11:
//       strcpy(monthasstring ,"Nov");
//       break;
//     case 12:
//       strcpy(monthasstring ,"Dec");
//       break;
//     default:
//       break;
//     }
//     RTCDateUpdate = String(monthasstring) + " "  + String(gps.date.day()) + " " + String(gps.date.year());
//     Serial.println(RTCDateUpdate);
//     RTCTimeUpdate = String(gps.time.hour()) + ":" + String(gps.time.minute()) + ":" + String(gps.time.second());
//     Serial.println(RTCTimeUpdate);
//     RTCdatetimesyncFlag = 0;
//     rtc.adjust(DateTime(RTCDateUpdate,RTCTimeUpdate));
//  }
  
//}
void displayInfo(){
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
 
}
/////////////////////////////////////////////////////////////////////////////////////

///////////////////////////SD Card Interfacing//////////////////////////////////////

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory"); 
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

void removeDir(fs::FS &fs, const char * path){
    Serial.printf("Removing Dir: %s\n", path);
    if(fs.rmdir(path)){
        Serial.println("Dir removed");
    } else {
        Serial.println("rmdir failed");
    }
}

void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("Read from file: ");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
    file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
  //  Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        //Serial.println("Failed to open file for appending");
      //  Serial.println(" ");
        return;
    }
    if(file.print(message)){
      //  Serial.println("Message appended");
      //  Serial.println(" ");
    } else {
        Serial.println("Append failed");
    }
    file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}

void testFileIO(fs::FS &fs, const char * path){
    File file = fs.open(path);
    static uint8_t buf[512];
    size_t len = 0;
    uint32_t start = millis();
    uint32_t end = start;
    if(file){
        len = file.size();
        size_t flen = len;
        start = millis();
        while(len){
            size_t toRead = len;
            if(toRead > 512){
                toRead = 512;
            }
            file.read(buf, toRead);
            len -= toRead;
        }
        end = millis() - start;
        Serial.printf("%u bytes read for %u ms\n", flen, end);
        file.close();
    } else {
        Serial.println("Failed to open file for reading");
    }


    file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }

    size_t i;
    start = millis();
    for(i=0; i<2048; i++){
        file.write(buf, 512);
    }
    end = millis() - start;
    Serial.printf("%u bytes written for %u ms\n", 2048 * 512, end);
    file.close();
}

////////////////////////////////////////////////////////////////////////////////

void setup(){
   
  Serial.begin(115200);
  gpsSerial.begin(9600, SERIAL_8N1, RXD1, TXD1);
  sysSerial.begin(9600, SERIAL_8N1, RXD2, TXD2);
   // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.print("Radiation Meter");
  pinMode(LCDbacklightcontrol,OUTPUT);
  digitalWrite(LCDbacklightcontrol,HIGH);
  delay(1000);
  digitalWrite(LCDbacklightcontrol,LOW);

  pinMode(bufferEnablecontrol,OUTPUT);
  digitalWrite(bufferEnablecontrol,LOW);


  if (! rtc.begin()) {     //RTC Initializing
    Serial.println("Couldn't find RTC");
    while (1);
  }
 // rtc.adjust(DateTime(__DATE__, __TIME__));
  //  rtc.adjust(DateTime("Jan 08 2020","17:14:15"));
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  if(!SD.begin()){         //SD Card module Initializing
        Serial.println("Card Mount Failed");
        return;
  }
  uint8_t cardType = SD.cardType();

  if(cardType == CARD_NONE){
    Serial.println("No SD card attached");
    return;
  }
  Serial.print("SD Card Type: ");
  if(cardType == CARD_MMC){
    Serial.println("MMC");
  } 
  else if(cardType == CARD_SD){
    Serial.println("SDSC");
  } 
  else if(cardType == CARD_SDHC){
    Serial.println("SDHC");
  } 
  else {
    Serial.println("UNKNOWN");
  }
  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);
    // listDir(SD, "/", 0);
    // createDir(SD, "/mydir");
    // listDir(SD, "/", 0);
    // removeDir(SD, "/mydir");
    // listDir(SD, "/", 2);
    // writeFile(SD, "/hello.txt", "Hello ");
    // appendFile(SD, "/hello.txt", "World!\n");
    // readFile(SD, "/hello.txt");
    // deleteFile(SD, "/foo.txt");
    // renameFile(SD, "/hello.txt", "/foo.txt");
    // readFile(SD, "/foo.txt");
    // testFileIO(SD, "/test.txt");
  Serial.printf("Total space: %lluMB\n", SD.totalBytes() / (1024 * 1024));
  Serial.printf("Used space: %lluMB\n", SD.usedBytes() / (1024 * 1024));

 // Initailse the pulse input pins as inputs
  pinMode(pulseIn1.PIN, INPUT_PULLUP);  
  pinMode(pulseIn2.PIN, INPUT_PULLUP);
  pinMode(pulseIn3.PIN, INPUT_PULLUP);
  pinMode(pulseIn4.PIN, INPUT_PULLUP);
  pinMode(pulseIn5.PIN, INPUT_PULLUP);
  pinMode(pulseIn6.PIN, INPUT_PULLUP);
  pinMode(pulseIn7.PIN, INPUT_PULLUP);
  pinMode(pulseIn8.PIN, INPUT_PULLUP);

  // Attach interrupt on falling edge, thus each pulse is counted at falling edge of the signal
  attachInterrupt(pulseIn1.PIN, isr1, FALLING);
  attachInterrupt(pulseIn2.PIN, isr2, FALLING);
  attachInterrupt(pulseIn3.PIN, isr3, FALLING);
  attachInterrupt(pulseIn4.PIN, isr4, FALLING);
  attachInterrupt(pulseIn5.PIN, isr5, FALLING);
  attachInterrupt(pulseIn6.PIN, isr6, FALLING);
  attachInterrupt(pulseIn7.PIN, isr7, FALLING);
  attachInterrupt(pulseIn8.PIN, isr8, FALLING);
   
}

void loop(){
  //////////////////// Count Reading //////////////////////////////////////////////////////
  unsigned long currentMillis = millis();

  DateTime now = rtc.now();
  if (currentMillis - previousMillis >= interval) {
    // print the number of count in the interval from all the pulse inputs
    Serial.printf("SerailData: %u %u %u %u %u %u %u %u %1f %1f %u %u %u %u %u %u \r\n", pulseIn1.numberOfPulses,pulseIn2.numberOfPulses,pulseIn3.numberOfPulses,pulseIn4.numberOfPulses,pulseIn5.numberOfPulses,
    pulseIn6.numberOfPulses,pulseIn7.numberOfPulses,pulseIn8.numberOfPulses,gps.location.lat(),gps.location.lng(),now.hour(),now.minute(),now.second(),now.day(),now.month(),now.year());
    
    sprintf(dataBuffer,"SD:%u %u %u %u %u %u %u %u %1f %1f %u %u %u %u %u %u \r\n",pulseIn1.numberOfPulses,pulseIn2.numberOfPulses,pulseIn3.numberOfPulses,pulseIn4.numberOfPulses,pulseIn5.numberOfPulses,
    pulseIn6.numberOfPulses,pulseIn7.numberOfPulses,pulseIn8.numberOfPulses,gps.location.lat(),gps.location.lng(),now.hour(),now.minute(),now.second(),now.day(),now.month(),now.year());
    // Reset the counter 
    pulseIn1.numberOfPulses = 0;
    pulseIn2.numberOfPulses = 0;
    pulseIn3.numberOfPulses = 0;
    pulseIn4.numberOfPulses = 0;
    pulseIn5.numberOfPulses = 0;
    pulseIn6.numberOfPulses = 0;
    pulseIn7.numberOfPulses = 0;
    pulseIn8.numberOfPulses = 0;

    // update the interval time
    previousMillis = currentMillis;
  }
///////////////////////////////////////////////////////////////////////////////////////////////////  

//////////////////// LCD Display controlling for GPS //////////////////////////////////////////////////////
   currentMillisLCD = millis();

  if (currentMillisLCD - previousMillisLCD >= intervalLCD) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(gps.location.lat(),6);
    lcd.setCursor(0, 1);
    lcd.print(gps.location.lng(),6);
    previousMillisLCD = currentMillisLCD;
  }
///////////////////////////////////////////////////////////////////////////////////////////////////  


///////////////////////// GPS Reading ////////////////////////////////////////////////////////////

  while (gpsSerial.available() >0) {
    // Serial.print(char(gpsSerial.read()));
    if (gps.encode(gpsSerial.read())){
      // displayInfo();
     // Serial.println(" ");
    }
  }
//////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////// Serial Reading and Writing ///////////////////////////////////////////

  while (Serial.available()) {
      //  Serial.print(char(Serial.read()));
        char c = Serial.read(); //Conduct a serial read
        if(c=='#'){
            SDfileReadingFlag = 1;
            break;
        } //Exit the loop when the /n is detected after the word
        SDfiletoreadSerial += c; //Shorthand for SDfiletoreadSerial = SDfiletoreadSerial + c
  }  
    if (SDfileReadingFlag == 1 && SDfiletoreadSerial.length() > 0) {
        ///332020
       // Serial.println(SDfiletoreadSerial);
       // SDfiletoreadSerial = '/' + SDfiletoreadSerial; 
       // SDfiletoreadSerialarray[0] = '\0';
        SDfiletoreadSerial.toCharArray(SDfiletoreadSerialarray, 15);
        lcd.clear();
        lcd.print("SD Card Reading");
        readFile(SD, SDfiletoreadSerialarray);
        SDfileReadingFlag = 0;
        SDfiletoreadSerial = "";
        SDfiletoreadSerialarray[0] = '\0';
    }



//////////////////////////////////////////////////////////////////////////////////////////////////
  

///////////////////////////// RTC Reading ////////////////////////////////////////////////////////
  if(RTCdatetimesyncFlag == 1){
    RTCDateTimeSync();
  }
  // //DateTime now = rtc.now();
  // Serial.print(now.hour(), DEC);
  // Serial.print(":");
  // Serial.print(now.minute(), DEC);
  // //delay(2000);
  // Serial.print(":");
  // Serial.println(now.second(), DEC);
  
  // //delay(2000);
 
  // Serial.print(now.day(), DEC);
  // Serial.print("-");
  // Serial.print(now.month(), DEC);
  // Serial.print("-");
  // Serial.println(now.year(), DEC);
  // Serial.println(daysOfTheWeek[now.dayOfTheWeek()]);
  //delay(2000);
  //delay(2000);
/////////////////////////////////////////////////////////////////////////////////////////////////
  dateSDcard = '/' + String(now.day()) + String(now.month()) + String(now.year());     //"/2112020"--filename
  //Serial.println(dateSDcard);     //Test
  dateSDcardarray[0] = '\0';
  dateSDcard.toCharArray(dateSDcardarray, 15);
  
  if(!(dateSDcard.equals(dateSDcardCompare))){    // SD card new file creating
    //dateSDcard = '/'+ dateSDcard;
    dateSDcardarray[0] = '\0';
    dateSDcard.toCharArray(dateSDcardarray, 15);
    //writeFile(SD, dateSDcardarray, " ");
    appendFile(SD, dateSDcardarray, " ");

  }
  appendFile(SD, dateSDcardarray, dataBuffer);          //Appending data should have '\n' at the end
  dateSDcard = "";
  dateSDcardCompare = "";
  dateSDcardCompare = '/' +String(now.day()) + String(now.month()) + String(now.year());
 // Serial.println(dateSDcardCompare);     //Test
 // delay(2000);
}

